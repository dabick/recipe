package recipe

import (
	"database/sql"
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/dabick/go-util/config"
	"gitlab.com/dabick/go-util/db"
	"gitlab.com/dabick/go-util/derr"
	"gitlab.com/dabick/go-util/log"
	"html/template"
	"net/http"
	"strconv"
)

type RecipeContainer struct {
	Recipes []Recipe
}

const (
	category = "recipe"
)

var (
	conn      *sql.DB
	templates = template.Must(template.ParseFiles("tmpl/index.tmpl"))
)

func init() {
	log.Println("Initializing Recipe.")
	config.InitConfig(category)
	db.InitDatabase(category)
	conn = db.GetDb(category)
}

func RegisterRecipe(router *mux.Router) {
	log.Println("Registering Recipe")

	baseSubrouter := router.PathPrefix("/recipes").Subrouter()

	subrouter := baseSubrouter.PathPrefix("/api").Subrouter()
	subrouter.Path("/").Methods("GET").HandlerFunc(searchRecipes)
	subrouter.Path("/").Methods("POST").HandlerFunc(addRecipe)
	subrouter.Path("/").Methods("PUT").HandlerFunc(updateRecipe)
	subrouter.Path("/{id:[0-9]+}").Methods("DELETE").HandlerFunc(deleteRecipe)
	subrouter.Path("/{id:[0-9]+}").Methods("GET").HandlerFunc(getRecipe)

	baseSubrouter.PathPrefix("/static/").Methods("GET").Handler(http.StripPrefix("/recipes/static/", http.FileServer(http.Dir("./recipes/"))))
	baseSubrouter.Path("/").Methods("GET").HandlerFunc(index)
	baseSubrouter.Methods("GET").HandlerFunc(index)
}

func index(w http.ResponseWriter, r *http.Request) {
	if r.URL.String() == "/recipes" {
		http.Redirect(w, r, "/recipes/", http.StatusMovedPermanently)
		return
	}

	if r.URL.String() != "/recipes/" {
		w.WriteHeader(404)
		w.Write([]byte("Page not found"))
		return
	}
	var recipes []Recipe
	var err error
	if recipes, err = getRecipes(r); err != nil {
		handleServerError(err, w)
		return
	}

	w.Header().Set("Cache-Control", "no-cache")

	if err := templates.ExecuteTemplate(w, "index.tmpl", RecipeContainer{recipes}); err != nil {
		log.Println(err)
	}
}

func searchRecipes(w http.ResponseWriter, r *http.Request) {
	log.Debug("Inside searchRecipes " + r.URL.String())
	recipes, err := getRecipes(r)
	if err != nil {
		handleServerError(err, w)
		return
	}

	if jsonBytes, err := json.Marshal(recipes); err == nil {
		w.Write(jsonBytes)
	} else {
		handleServerError(err, w)
	}
}

func getRecipes(r *http.Request) ([]Recipe, error) {
	var rows *sql.Rows
	var err error
	if rows, err = getRowsForRecipeSearch(r); err != nil {
		return nil, err
	}
	defer rows.Close()

	var recipes []Recipe

	for rows.Next() {
		var recipe Recipe
		if err := rows.Scan(&recipe.Id, &recipe.SubmitterId, &recipe.AuthorName, &recipe.Name, &recipe.Description); err != nil {
			return nil, err
		}

		recipe.Ingredients = getIngredients(recipe.Id)
		recipe.Instructions = getInstructions(recipe.Id)

		recipes = append(recipes, recipe)
	}
	return recipes, nil
}

func getRowsForRecipeSearch(r *http.Request) (*sql.Rows, error) {
	if err := r.ParseForm(); err != nil {
		return nil, err
	}

	if id, err := strconv.ParseUint(r.Form.Get("previousId"), 10, 64); err == nil && id > 0 {
		return conn.Query(getRecipeSearchSql(id), id)
	} else {
		return conn.Query(getRecipeSearchSql(0))
	}
}

func getRecipeSearchSql(previousId uint64) string {
	recipeSql := "SELECT r.id, r.submitterId, u.name, r.name, r.description " +
		" FROM recipes r " +
		" JOIN users u ON " +
		"      u.id = r.submitterId "
	if previousId > 0 {
		recipeSql += " WHERE r.id > $1 "
	}
	recipeSql += " ORDER BY id LIMIT 10 "
	return recipeSql
}

func handleError(err *derr.WebError, w http.ResponseWriter) {
	log.Println(err.Message)
	w.WriteHeader(err.Code)
	w.Write([]byte(err.Clean))
}

func addRecipe(w http.ResponseWriter, r *http.Request) {
	recipe, recipeErr := parse(r)
	if recipeErr != nil {
		handleError(recipeErr, w)
		return
	}

	if recipe.SubmitterId <= 0 || len(recipe.Name) == 0 || len(recipe.Description) == 0 {
		log.Println("Required fields were not passed in.")
		w.WriteHeader(400)
		return
	}

	validateRecipe(recipe)

	tran, err := conn.Begin()
	if err != nil {
		handleServerError(err, w)
		return
	}

	err = tran.QueryRow(
		"INSERT INTO recipes (submitterId, name, description) VALUES ($1, $2, $3) RETURNING id",
		recipe.SubmitterId,
		recipe.Name,
		recipe.Description).Scan(&recipe.Id)
	if err != nil {
		handleErrorInTransaction(tran, err, w)
		return
	}

	if err = insertAllChildren(recipe, tran); err != nil {
		handleErrorInTransaction(tran, err, w)
		return
	}

	if err = tran.QueryRow("SELECT name FROM users WHERE id = $1", recipe.SubmitterId).Scan(&recipe.AuthorName); err != nil {
		tran.Rollback()
		log.Println(err)
		w.WriteHeader(400)
		return
	}

	recipeJson, err := json.Marshal(recipe)
	if err != nil {
		handleErrorInTransaction(tran, err, w)
	} else {
		w.Write(recipeJson)
		tran.Commit()
	}
}

func insertAllChildren(recipe *Recipe, tran *sql.Tx) error {
	if err := insertChildren("ingredients", "name", recipe.Id, tran, recipe.Ingredients); err != nil {
		return err
	}
	return insertChildren("instructions", "details", recipe.Id, tran, recipe.Instructions)
}

func insertChildren(tableName string, column string, recipeId uint64, tran *sql.Tx, values []string) error {
	query := "INSERT INTO " + tableName + " (id, " + column + ", recipeId) VALUES ($1, $2, $3)"
	stmt, err := tran.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()
	for i, value := range values {
		_, err = stmt.Exec(i, value, recipeId)
		if err != nil {
			return err
		}
	}
	return nil
}

func handleServerError(err error, w http.ResponseWriter) {
	log.Println(err)
	w.WriteHeader(500)
}

func updateRecipe(w http.ResponseWriter, r *http.Request) {
	recipe, recipeErr := unmarshalRecipe(r)
	if recipeErr != nil {
		handleError(recipeErr, w)
		return
	}

	if recipe.Id <= 0 {
		w.WriteHeader(400)
		return
	}

	tran, err := conn.Begin()
	if err != nil {
		handleServerError(err, w)
		return
	}

	_, err = tran.Exec("UPDATE recipes SET name = $1, description = $2 WHERE id = $3", recipe.Name, recipe.Description, recipe.Id)
	if err != nil {
		handleErrorInTransaction(tran, err, w)
		return
	}

	if err = updateChildren(recipe, tran); err != nil {
		handleErrorInTransaction(tran, err, w)
	} else {
		tran.Commit()
		w.WriteHeader(204)
	}
}

func updateChildren(recipe *Recipe, tran *sql.Tx) error {
	if err := deleteChildren("ingredients", recipe.Id, tran); err != nil {
		return err
	}
	if err := deleteChildren("instructions", recipe.Id, tran); err != nil {
		return err
	}
	return insertAllChildren(recipe, tran)
}

func deleteChildren(tableName string, recipeId uint64, tran *sql.Tx) error {
	_, err := tran.Exec("DELETE FROM "+tableName+" WHERE recipeId = $1", recipeId)
	return err
}

func handleErrorInTransaction(tran *sql.Tx, err error, w http.ResponseWriter) {
	tran.Rollback()
	handleServerError(err, w)
}

func getRecipe(w http.ResponseWriter, r *http.Request) {
	id := getIdFromRequest(w, r)
	if id == 0 {
		return
	}

	recipe, err := getRecipeForId(id)
	switch {
	case err == sql.ErrNoRows:
		w.WriteHeader(400)
		w.Write([]byte("No recipe found."))
	case err != nil:
		handleServerError(err, w)
	default:
		convertRecipe(recipe, w)
	}
}

func getIdFromRequest(w http.ResponseWriter, r *http.Request) uint64 {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte("Valid integer > 0 required for 'id'."))
		id = 0
	}
	return id
}

func convertRecipe(recipe *Recipe, w http.ResponseWriter) {
	recipe.Ingredients = getIngredients(recipe.Id)
	recipe.Instructions = getInstructions(recipe.Id)

	recipeJson, err := json.Marshal(recipe)
	if err != nil {
		handleServerError(err, w)
		return
	}
	w.Write(recipeJson)
}

func getIngredients(recipeId uint64) []string {
	return getArrayForRecipe("SELECT name FROM ingredients WHERE recipeId = $1 ORDER BY id", recipeId)
}

func getInstructions(recipeId uint64) []string {
	return getArrayForRecipe("SELECT details FROM instructions WHERE recipeId = $1 ORDER BY id", recipeId)
}

func getArrayForRecipe(query string, recipeId uint64) []string {
	rows, err := conn.Query(query, recipeId)
	if err != nil {
		log.Println(err)
		return make([]string, 0)
	}
	defer rows.Close()

	var results []string
	for rows.Next() {
		var name string
		if err = rows.Scan(&name); err != nil {
			log.Println(err)
			continue
		}
		results = append(results, name)
	}
	return results
}

func getRecipeForId(recipeId uint64) (*Recipe, error) {
	recipeSql := "SELECT r.id, r.submitterId, u.name, r.name, r.description " +
		" FROM recipes r " +
		" JOIN users u ON " +
		"      u.id = r.submitterId " +
		" WHERE r.id = $1"
	query, err := conn.Prepare(recipeSql)
	if err != nil {
		return nil, err
	}

	defer query.Close()

	row := query.QueryRow(recipeId)
	var recipe Recipe
	if err = row.Scan(&recipe.Id, &recipe.SubmitterId, &recipe.AuthorName, &recipe.Name, &recipe.Description); err != nil {
		return nil, err
	}
	return &recipe, nil
}

func deleteRecipe(w http.ResponseWriter, r *http.Request) {
	id := getIdFromRequest(w, r)
	if id == 0 {
		return
	}

	tran, err := conn.Begin()
	if err != nil {
		handleServerError(err, w)
	}

	if delete(tran, id, "ingredients", "recipeId", w) &&
		delete(tran, id, "instructions", "recipeId", w) &&
		delete(tran, id, "recipes", "id", w) {
		tran.Commit()
		w.WriteHeader(204)
	}
}

func delete(tran *sql.Tx, id uint64, table string, column string, w http.ResponseWriter) bool {
	if _, err := tran.Exec("DELETE FROM "+table+" WHERE "+column+" = $1", id); err != nil {
		handleErrorInTransaction(tran, err, w)
		return false
	}
	return true
}
