package recipe

import (
	"encoding/json"
	"gitlab.com/dabick/go-util/derr"
	"io/ioutil"
	"net/http"
	"strconv"
	"text/template"
)

type Recipe struct {
	Id           uint64   `json:"id"`
	SubmitterId  uint     `json:"authorId"`
	AuthorName   string   `json:"author"`
	Name         string   `json:"name"`
	Description  string   `json:"description"`
	Instructions []string `json:"instructions"`
	Ingredients  []string `json:"ingredients"`
}

func getError(status int, err error) *derr.WebError {
	if err == nil {
		return nil
	}
	return &derr.WebError{Code: status, Message: err.Error(), Clean: ""}
}

func parse(r *http.Request) (*Recipe, *derr.WebError) {
	recipe, err := unmarshalRecipe(r)
	if err != nil {
		return nil, err
	}

	recipe, err = validateRecipe(recipe)
	if err != nil {
		return nil, err
	}

	return recipe, nil
}

func unmarshalRecipe(r *http.Request) (*Recipe, *derr.WebError) {
	defer r.Body.Close()

	recipeString, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, getError(500, err)
	}

	var recipe Recipe
	err = json.Unmarshal(recipeString, &recipe)
	return &recipe, getError(415, err)
}

func validateRecipe(recipe *Recipe) (*Recipe, *derr.WebError) {
	if isEmpty(recipe.Name) || isEmpty(recipe.Description) {
		return validationError("Name and Description are required.")
	}

	if len(recipe.Name) > 70 {
		return validationError("Name cannot be more than 70 characters.")
	} else if len(recipe.Description) > 1000 {
		return validationError("Description cannot be more than 1000 characters.")
	} else if err := validateSlice(recipe.Instructions, 200, "Instructions"); err != nil {
		return nil, err
	} else if err := validateSlice(recipe.Ingredients, 100, "Ingredients"); err != nil {
		return nil, err
	}

	recipe.Name = sanitizeHtml([]string{recipe.Name})[0]
	recipe.Description = sanitizeHtml([]string{recipe.Description})[0]
	recipe.Instructions = sanitizeHtml(recipe.Instructions)
	recipe.Ingredients = sanitizeHtml(recipe.Ingredients)

	return recipe, nil
}

func validateSlice(pieces []string, length int, entity string) *derr.WebError {
	for _, piece := range pieces {
		if len(piece) > length {
			_, err := validationError(entity + " cannot be more than " + strconv.Itoa(length) + " charaters per line.")
			return err
		}
	}
	return nil
}

func validationError(message string) (*Recipe, *derr.WebError) {
	return nil, &derr.WebError{Code: 400, Message: message, Clean: message}
}

func isEmpty(value string) bool {
	if len(value) == 0 {
		return true
	}
	return false
}

func sanitizeHtml(uncleans []string) []string {
	clean := make([]string, len(uncleans))
	for i, unclean := range uncleans {
		clean[i] = template.HTMLEscapeString(unclean)
	}
	return clean
}
