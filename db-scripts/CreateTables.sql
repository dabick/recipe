CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    authSalt bytea,
    authIv bytea,
    authToken bytea,
    password CHAR(60) NOT NULL
);

CREATE TABLE recipes (
    id SERIAL PRIMARY KEY,
    submitterId INT NOT NULL REFERENCES users (id),
    name VARCHAR(70) NOT NULL,
    description VARCHAR(1000) NOT NULL
);

CREATE TABLE ingredients (
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    recipeId INT NOT NULL REFERENCES recipes (id),
    PRIMARY KEY (id, recipeId)
);

CREATE TABLE instructions (
    id SMALLINT NOT NULL,
    details VARCHAR(200) NOT NULL,
    recipeId INT NOT NULL REFERENCES recipes (id),
    PRIMARY KEY (id, recipeId)
);