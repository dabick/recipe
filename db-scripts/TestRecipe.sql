insert into users (name, email, password) values ('test', '', 'test');

insert into recipes (submitterId, name, description) values (1, 'Tasty Test', 'This is a test recipe.');

insert into ingredients (id, name, recipeId) values (1, '10 pounds of cooked bacon', 1);

insert into instructions (id, details, recipeId) values (1, 'Enjoy', 1);