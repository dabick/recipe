'use strict'

var recipes = {};
(function namespace() {
    var Ingredients = React.createClass({
        render: function() {
            var ingredientNodes = this.props.ingredients.map(function(ingredient, index) {
                if (!ingredient) {
                    return
                }
                return <li key={index}>{ingredient}</li>
            });
            return (
                <div className='ingredients'>
                    <h3>Ingredients</h3>
                    <ul>{ingredientNodes}</ul>
                </div>
            );
        }
    });

    var Instructions = React.createClass({
        render: function() {
            var instructionNodes = this.props.instructions.map(function(instruction, index) {
                if (!instruction) {
                    return;
                }
                return <li key={index}>{instruction}</li>
            });
            return (
                <div className='instructions'>
                    <h3>Instructions</h3>
                    <ol>{instructionNodes}</ol>
                </div>
            );
        }
    })

    recipes.Recipe = React.createClass({
        getInitialState: function() {
            return {
                edit: false,
                add: false,
                recipe: {
                    ingredients: [],
                    instructions: []
                }
            };
        },
        componentWillMount: function() {
            if (!this.props.id) {
                this.handleAddClick();
                return;
            }
            ajaxRequest(
                this.props.apiUrl + this.props.id,
                'GET',
                function(newRecipe) {
                    this.setState({recipe: newRecipe});
                }.bind(this),
                function(errorStatus, errorBody) {
                    console.log(errorBody);
                    alert(errorStatus);
                }
            );
        },
        handleEditClick: function (e) {
            this.setState({edit: true});
        },
        handleAddClick: function(e) {
            this.setState({edit: true, add: true});
        },
        handleSaveClick: function(e) {
            var updatedRecipe = {
                id: this.state.add ? 0 : this.state.recipe.id,
                //todo this.state.add ? GET_AUTHOR_ID : this.state.recipe.id,
                authorId: 1,
                name: document.getElementById('recipeName').value,
                description: document.getElementById('recipeDescription').value,
                ingredients: document.getElementById('recipeIngredients').value.split('\n'),
                instructions: document.getElementById('recipeInstructions').value.split('\n')
            }
            ajaxRequest(
                this.props.apiUrl,
                this.state.add ? 'POST' : 'PUT',
                function(updatedRecipe, newRecipe) {
                    var recipe = this.state.add ? newRecipe : updatedRecipe;
                    this.setState({edit: false, add: false, recipe: recipe});
                }.bind(this, updatedRecipe),
                function(errorStatus, errorBody) {
                    alert(errorStatus);
                },
                updatedRecipe
            );
        },
        handleDeleteClick: function(e) {
            //todo confirmation before deleting?
            ajaxRequest(
                this.props.apiUrl + this.state.recipe.id,
                'DELETE',
                function(status) {
                    this.handleHomeClick(e);
                }.bind(this),
                function(status, error) {
                    console.log(error);
                    alert(status);
                }
            );
        },
        handleHomeClick: function(e) {
            window.location = "/recipes/";
        },
        handleCancelClick: function(e) {
            this.setState({add: false, edit: false});
        },
        render: function() {
            var recipe = this.state.add ? {ingredients: [], instructions: []} : this.state.recipe;
            if (this.state.edit) {
                var ingredients = recipe.ingredients.join("\n");
                var instructions = recipe.instructions.join("\n");
                return (
                    <div className="recipe">
                        Recipe: <input id="recipeName" defaultValue={recipe.name}/>
                        <br/>Description: <textarea id='recipeDescription' defaultValue={recipe.description} />
                        <br/>Ingredients<textarea id='recipeIngredients'
                            placeholder="Each ingredient should go on its own line." defaultValue={ingredients} />
                        <br/>Instructions<textarea id='recipeInstructions'
                            placeholder="Each instruction should go on its own line." defaultValue={instructions} />
                        <br/><button onClick={this.handleSaveClick}>Save</button>
                        <button onClick={this.handleCancelClick}>Cancel</button>
                    </div>
                );
            } else {
                return (
                    <div className="recipe">
                        <button onClick={this.handleHomeClick}>Home</button>
                        <br/>
                        <button onClick={this.handleAddClick}>Add</button>
                        <button onClick={this.handleEditClick}>Edit</button>
                        <button onClick={this.handleDeleteClick}>Delete</button>
                        <br/>
                        <h2>{recipe.name}</h2>
                        <p>{recipe.description}</p>
                        <Ingredients ingredients={recipe.ingredients}/>
                        <Instructions instructions={recipe.instructions}/>
                    </div>
                );
            }
        }
    });

    recipes.render = function() {
        var initialId = getQueryParameter('recipe');
        ReactDOM.render(
            <recipes.Recipe id={initialId} apiUrl={'/recipes/api/'} />,
            document.getElementById('content')
        );
    }
}());

recipes.render();

window.onbeforeunload = function(e) {
    ReactDOM.unmountComponentAtNode(document.getElementById('content'));
}